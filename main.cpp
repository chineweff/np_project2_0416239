#include <iostream>
#include <string>
#include <map>
#include <unistd.h>
#include <sys/wait.h>
#include <sstream>
#include <vector>
#include <string.h>
#include <cstdlib>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/stat.h>
#include <arpa/inet.h>
#include <netdb.h>
#define demo 1
#define server_port 9995
using namespace std;

struct data
{
    int ID;
    int command_num;
    string name;
    string IP;
    string port;
    map<string, string> env;
    map<int, string>find_tmp;
    vector<string> pipe;
};
map<int, data> cli_info; // client sockfd to client info
string filename;
char go;
int newsockfd, sockfd;
int exec_fd, send_fd, recv_fd;
int pipe_num, send_num, recv_num;
bool rrecv, ssend;
bool first_command, command_not_found;
bool used_ID[30] = {0};
fd_set rfds;
fd_set afds;
string bad;
void launch(char** command)
{
    if((string)command[0] == "exit")
    {
        for(auto &it : cli_info[exec_fd].find_tmp) remove(it.second.c_str());
        stringstream tmp;
        tmp << "*** User '" << cli_info[exec_fd].name << "' left. ***\n";
        for(auto &it : cli_info)
        {
            //cout << it.second.ID << endl;
            it.second.pipe[cli_info[exec_fd].ID].clear();
            write(it.first, tmp.str().c_str(), tmp.str().size());
        }
        used_ID[cli_info[exec_fd].ID - 1] = false;
        close(exec_fd);
        FD_CLR(exec_fd, &afds);
        cli_info.erase(exec_fd);
        return;
    }

    if((string)command[0] == "setenv")
    {
        cli_info[exec_fd].env[ (string)command[1] ] = (string)command[2];
        return;
    }
    else if((string)command[0] == "printenv")
    {
        stringstream ss_env;
        ss_env << (string)command[1] << "=";

        for(auto &it : cli_info[exec_fd].env)
        {
            if(it.first == (string)command[1])
            {
                ss_env << it.second << "\n";
                write(exec_fd, ss_env.str().c_str(), ss_env.str().size());
                return;
            }
        }
        ss_env << "\n";
        write(exec_fd, ss_env.str().c_str(), ss_env.str().size());
        return;
    }

    int command_num = cli_info[exec_fd].command_num;

    if(go == '|' && cli_info[exec_fd].find_tmp.find(pipe_num+command_num) == cli_info[exec_fd].find_tmp.end() )
    {
        int fd;
        char tmpfile[] = {"/tmp/0416239XXXXXX"};
        fd = mkstemp(tmpfile);
        cli_info[exec_fd].find_tmp.insert(pair<int, string >(pipe_num+command_num, (string)tmpfile));
        close(fd);
    }

    pid_t pid, wpid;
    int status;
    int recv_pipefd[2];
    int send_pipefd[2];
    pipe(recv_pipefd);
    pipe(send_pipefd);

    pid = fork();

    if( pid == 0) // child
    {
        // change result to client terminal
        dup2(exec_fd, STDOUT_FILENO);
        dup2(exec_fd, STDERR_FILENO);

        // personal pipe
        if(go == '|')
        {
            freopen(cli_info[exec_fd].find_tmp[command_num+pipe_num].c_str(), "a", stdout);
        }
        // redirection
        else if(go == '>')
        {
            freopen(filename.c_str(), "w", stdout);
        }
        // pipe to other client
        else if(ssend)
        {
            close(send_pipefd[0]);
            dup2(send_pipefd[1], STDOUT_FILENO);
            dup2(send_pipefd[1], STDERR_FILENO);
            close(send_pipefd[1]);
        }

        // recv from other client
        if(rrecv)
        {
            close(recv_pipefd[1]);
            dup2(recv_pipefd[0], STDIN_FILENO);
            close(recv_pipefd[0]);
        }

        // if there's a pipe tmpfile use it as input
        else if(cli_info[exec_fd].find_tmp.find(command_num) != cli_info[exec_fd].find_tmp.end() )
            freopen(cli_info[exec_fd].find_tmp[command_num].c_str(), "r", stdin);

        // change env to client env
        for(auto &it : cli_info[exec_fd].env)
            setenv(it.first.c_str(), it.second.c_str(), true);

        if( execvp(command[0], command) == -1 )
        {
            dup2(exec_fd, STDOUT_FILENO);
            dup2(exec_fd, STDERR_FILENO);
            // exec failed do not creat pipe tmpfile
            if(go == '|')
            {
                struct stat statbuf;
                stat(cli_info[exec_fd].find_tmp[command_num+pipe_num].c_str(), &statbuf);

                // if there's a pipe tmpfile before exec failed do not delete it
                if(statbuf.st_size == 0)
                {
                    remove(cli_info[exec_fd].find_tmp[command_num+pipe_num].c_str());
                    cli_info[exec_fd].find_tmp.erase(command_num+pipe_num);
                }
            }

            // exec failed do not creat redirection file
            else if(go == '>') remove(filename.c_str());
            cout << "Unknown command: [" << (string)command[0] << "].\n";
        }
        exit(EXIT_FAILURE);
    }
    else if ( pid < 0) perror("fork error");

    else // parent
    {
        if(rrecv)
        {
            close(recv_pipefd[0]);
            write(recv_pipefd[1], cli_info[recv_fd].pipe[cli_info[exec_fd].ID].c_str(), cli_info[recv_fd].pipe[cli_info[exec_fd].ID].size());
            close(recv_pipefd[1]);
            cli_info[recv_fd].pipe[ cli_info[exec_fd].ID ].clear();

            for(auto &it : cli_info)
            {
                stringstream tmp;
                tmp << "*** " << cli_info[exec_fd].name << " (#" << cli_info[exec_fd].ID << ") just received from " << cli_info[recv_fd].name << " (#" << recv_num << ") by '" << bad << "' ***\n";
                write(it.first, tmp.str().c_str(), tmp.str().size());
            }
        }
        if(ssend)
        {
            char buf[1025];
            bzero(buf, 1025);
            close(send_pipefd[1]);
            read(send_pipefd[0], buf, 1025);
            close(send_pipefd[0]);
            cli_info[exec_fd].pipe[send_num] = (string)buf;
            for(auto &it : cli_info)
            {
                stringstream tmp;
                tmp << "*** " << cli_info[exec_fd].name << " (#" << cli_info[exec_fd].ID << ") just piped '" << bad << "' to " << cli_info[send_fd].name << " (#" << send_num << ") ***\n";
                write(it.first, tmp.str().c_str(), tmp.str().size());
            }
        }
        int receive_status;
        waitpid(pid, &status, 0);
        if(WIFEXITED(status))  receive_status = WEXITSTATUS(status);

        if(receive_status == EXIT_FAILURE)
        {
            command_not_found = true;
            if(!first_command) --cli_info[exec_fd].command_num;
        }
        ssend = rrecv = false;

        if(cli_info[exec_fd].find_tmp.find(command_num) != cli_info[exec_fd].find_tmp.end() )
        {
            remove(cli_info[exec_fd].find_tmp[command_num].c_str());
            cli_info[exec_fd].find_tmp.erase(command_num);
        }
    }
}
void part(vector<char*> parse)
{
    if(ssend)
    {
        send_fd == -1;
        for(auto &it : cli_info)
            if(it.second.ID == send_num)
            {
                send_fd = it.first;
                break;
            }
        if(send_fd == -1 || send_num > cli_info.size())
        {
            stringstream tmp;
            tmp << "*** Error: user #" << send_num << " does not exist yet. ***\n";
            write(exec_fd, tmp.str().c_str(), tmp.str().size());
            if(rrecv == false) return;
        }
        if(!cli_info[exec_fd].pipe[send_num].empty() )
        {
            stringstream tmp;
            tmp << "*** Error: the pipe #" << cli_info[exec_fd].ID << "->#" << send_num << " already exists. ***\n";
            write(exec_fd, tmp.str().c_str(), tmp.str().size());
            if(rrecv == false) return;
        }
    }
    if(rrecv)
    {
        recv_fd = -1;
        for(auto &it : cli_info)
            if(it.second.ID == recv_num)
            {
                recv_fd = it.first;
                break;
            }
        if(recv_fd == -1 || recv_num > cli_info.size() || cli_info[recv_fd].pipe[cli_info[exec_fd].ID].empty())
        {
            stringstream tmp;
            tmp << "*** Error: the pipe #" << recv_num << "->#" << cli_info[exec_fd].ID << " does not exist yet. ***\n";
            write(exec_fd, tmp.str().c_str(), tmp.str().size());
            rrecv = false;
            command_not_found = true;
            return;
        }
    }
    if(parse.empty()) return;
    cli_info[exec_fd].command_num++;
    parse.push_back(NULL);
    char** command = parse.data();
    launch(command);
    first_command = false;
}
void shell(string input)
{
    bad = input;
    if(bad[bad.size()-1] == '\n') bad.pop_back();
    if(bad[bad.size()-1] == '\r') bad.pop_back();
    if(input.empty()) return;

    vector<char*> parse;
    stringstream ss(input);
    ssend = rrecv = command_not_found = false;
    first_command = true;
    while(1)
    {
        if(command_not_found) break;
        go = 0;
        string tmp;
        ss >> tmp;
        if(first_command == true)
        {
            if(tmp == "name")
            {
                string n;
                ss >> n;
                for(auto &it : cli_info)
                {
                    if(it.first != exec_fd && n == it.second.name)
                    {
                        stringstream tmp;
                        tmp << "*** User '" << n << "' already exists. ***\n";
                        write(exec_fd, tmp.str().c_str(), tmp.str().size());
                        return;
                    }
                }
                for(auto &it : cli_info)
                {
                    stringstream ss_name;
                    ss_name << "*** User from " << cli_info[exec_fd].IP << "/" << cli_info[exec_fd].port << " is named '" << n << "'. ***\n";
                    write(it.first, ss_name.str().c_str(), ss_name.str().size());
                }
                cli_info[exec_fd].name = n;
                return;
            }
            if(tmp == "who")
            {
                stringstream tmp;
                tmp << "<ID>\t<nickname>\t<IP/port>\t<indicate me>\n";
                write(exec_fd, tmp.str().c_str(), tmp.str().size());
                for(auto &it : cli_info)
                {
                    stringstream tmpp;
                    tmpp << it.second.ID << "\t" << it.second.name << "\t" << it.second.IP << "/" << it.second.port;
                    if(it.first == exec_fd) tmpp << "\t<-me\n";
                    else tmpp << "\n";
                    write(exec_fd, tmpp.str().c_str(), tmpp.str().size());
                }
                return;
            }
            if(tmp == "tell")
            {
                int id;
                ss >> id;
                for(auto &it : cli_info)
                {
                    if(it.second.ID == id)
                    {
                        char msg[1025];
                        ss.get(msg, 1025);
                        stringstream tell;
                        tell << "*** " << cli_info[exec_fd].name << " told you ***: " << msg << "\n";
                        write(it.first, tell.str().c_str(), tell.str().size());
                        return;
                    }
                }
                stringstream noid;
                noid << "*** Error: user #" << id << " does not exist yet. ***\n";
                write(exec_fd, noid.str().c_str(), noid.str().size());
                return;
            }
            if(tmp == "yell")
            {
                char msg[1025];
                ss.get(msg, 1025);
                for(auto &it : cli_info)
                {
                    stringstream yell;
                    yell << "*** " << cli_info[exec_fd].name << " yelled ***: " << msg << "\n";
                    write(it.first, yell.str().c_str(), yell.str().size());
                }
                return;
            }
        }
        if(tmp == "")
        {
            go = '\0';
            part(parse);
            break;
        }
        if(tmp[0] == '|')
        {
            go = tmp[0];
            tmp.erase(0,1);
            pipe_num = tmp.empty() ? 1 : stoi(tmp);
            part(parse);
            parse.clear();
            continue;
        }
        if(tmp == ">")
        {
            go = tmp[0];
            ss >> filename;
            part(parse);
            break;
        }
        if(tmp[0] == '>')
        {
            ssend = true;
            tmp.erase(0,1);
            send_num = stoi(tmp);
            ss >> tmp;
            if(tmp[0] == '<')
            {
                rrecv = true;
                tmp.erase(0,1);
                recv_num = stoi(tmp);
            }
            part(parse);
            break;
        }
        if(tmp[0] == '<')
        {
            rrecv = true;
            tmp.erase(0,1);
            recv_num = stoi(tmp);
            continue;
        }
        char* qq = new char[tmp.length()+1];
        strcpy(qq, tmp.c_str());
        parse.push_back(qq);
    }
}
void child_exit(int)
{
    wait(nullptr);
}
void parent_exit(int)
{
    for(auto &it : cli_info)
        for(auto &itt : it.second.find_tmp)
            remove(itt.second.c_str());
    close(sockfd);
    close(newsockfd);
    exit(0);
}
void init_server(struct sockaddr_in serv_addr)
{
     sockfd = socket(AF_INET, SOCK_STREAM, 0);
     if (sockfd < 0) perror("ERROR opening socket");
     bzero((char *) &serv_addr, sizeof(serv_addr));
     serv_addr.sin_family = AF_INET;
     serv_addr.sin_addr.s_addr = INADDR_ANY;
     serv_addr.sin_port = htons(server_port);
     if (bind(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0) perror("ERROR on binding");
     listen(sockfd,5);
}
void greeting(int fd)
{
    char msg[] = { "****************************************\n"
                   "** Welcome to the information server. **\n"
                   "****************************************\n" };
    write(fd, msg, strlen(msg));
}
int main(int argc, char *argv[])
{
     signal (SIGCHLD, child_exit);
     signal (SIGINT, parent_exit);

     string ini_dir = (string)getenv("HOME") + "/ras";
     chdir(ini_dir.c_str());
     setenv("PATH", "bin:." , true);

     struct sockaddr_in serv_addr;
     init_server(serv_addr);

     struct sockaddr_in cli_addr;
     socklen_t clilen;
     int fd, nfds;
     int identify = 1;
     nfds = getdtablesize();
     FD_ZERO(&afds);
     FD_SET(sockfd, &afds);
     while(1)
     {
        memcpy(&rfds, &afds, sizeof(rfds));
        if(select(nfds, &rfds, (fd_set*)0, (fd_set*)0, (struct timeval*)0) < 0)
        {
            perror("ERROR on select");
            exit(0);
        }
        if(FD_ISSET(sockfd, &rfds)) // someone connect
        {
            clilen = sizeof(cli_addr);
            newsockfd = accept(sockfd, (struct sockaddr *) &cli_addr, &clilen);
            if(newsockfd < 0)
            {
                perror("ERROR on accept");
                exit(0);
            }
            FD_SET(newsockfd, &afds);
            greeting(newsockfd);
            // get new user info and change form
            char hbuf[NI_MAXHOST], sbuf[NI_MAXSERV];
            int err = getnameinfo((struct sockaddr*)&cli_addr, clilen, hbuf, sizeof(hbuf), sbuf, sizeof(sbuf), 0);
            if(demo)
            {
                bzero(hbuf, NI_MAXHOST);
                bzero(sbuf, NI_MAXSERV);
                sprintf(hbuf, "CGILAB");
                sprintf(sbuf, "511");
            }
            stringstream ss_enter;
            ss_enter << "*** User '(no name)' entered from " << hbuf << "/" << sbuf << ". ***\n";

            // add to vector

            data ini_info;
            for(int i = 0; i < 30; ++i)
            {
                if(used_ID[i] == false)
                {
                    ini_info.ID = i + 1;
                    used_ID[i] = true;
                    break;
                }
            }
            ini_info.command_num = 0;
            ini_info.name = "(no name)";
            ini_info.IP = (string)hbuf;
            ini_info.port = (string)sbuf;
            ini_info.env["PATH"] = "bin:.";
            ini_info.pipe.resize(31);
            for(auto &it : ini_info.pipe) it.clear();
            cli_info.insert(pair<int, data>(newsockfd, ini_info));

            //broadcast all user someone connected
            for(auto &it : cli_info)
                write(it.first, ss_enter.str().c_str(), ss_enter.str().size());
            write(newsockfd, "% ", 2);
        }
        for(auto &it : cli_info)
        {
            if(FD_ISSET(it.first, &rfds))
            {
                char buf[1025];
                bzero(buf, 1025);
                int cc;
                cc = recv(it.first, buf, sizeof(buf), 0);
                if(cc < 0) perror("ERROR on read");
                else if(cc == 0)
                {
                    close(fd);
                    FD_CLR(fd, &afds);
                }
                else
                {
                    exec_fd = it.first;
                    shell((string)buf);
                    write(it.first, "% ", 2);
                }
            }
        }
     }
     return 0;
}
