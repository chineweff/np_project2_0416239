#include <iostream>
#include <sys/mman.h>
#include <string>
#include <map>
#include <unistd.h>
#include <sys/wait.h>
#include <sstream>
#include <vector>
#include <string.h>
#include <cstdlib>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/stat.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <boost/interprocess/managed_shared_memory.hpp>
#include <boost/interprocess/containers/map.hpp>
#include <boost/interprocess/allocators/allocator.hpp>
#include <functional>
#include <utility>
#define MAX_MESSAGE 1024
#define MAX_CLIENTS 30
#define demo 1
#define server_port 9995

void send_message(const char *message, int fd);
void close_child_fd(int fd);
struct data
{
    int ID;
    std::string name;
    std::string IP;
    std::string port;
    std::map<std::string, std::string> env;
    std::map<int, std::string>find_tmp;
};

////////////////////////////////////////////////////////////////////////////////////////////////////////
// allocator for creating stl map in shared memory
using namespace boost::interprocess;
struct shm_remove
{
     shm_remove() { shared_memory_object::remove("MySharedMemory"); }
    ~shm_remove(){ shared_memory_object::remove("MySharedMemory"); }
} remover;
managed_shared_memory segment(create_only,"MySharedMemory", 65536);
typedef int   KeyType;
typedef data  MappedType;
typedef std::pair<const int, data> ValueType;
typedef boost::interprocess::allocator<ValueType, managed_shared_memory::segment_manager> ShmemAllocator;
typedef boost::interprocess::map<KeyType, MappedType, std::less<KeyType>, ShmemAllocator> MyMap;
ShmemAllocator alloc_inst (segment.get_segment_manager());
MyMap *mymap = segment.construct<MyMap> ("MyMap") (std::less<int>(), alloc_inst);
offset_ptr<MyMap> cli_info = segment.find<MyMap>("MyMap").first;
////////////////////////////////////////////////////////////////////////////////////////////////////////
using namespace std;

pid_t master_pid;
string filename, input;
char go;
int newsockfd, sockfd;
int exec_fd, send_fd, recv_fd;
int pipe_num, send_num, recv_num;
int command_num;
bool rrecv, ssend, first_command, command_not_found;
bool *used_ID, *sending, *locking;
char *write_msg;
int *write_to;
void launch(char** command)
{

    if((string)command[0] == "setenv")
    {
        (*cli_info)[exec_fd].env[ (string)command[1] ] = (string)command[2];
        return;
    }
    else if((string)command[0] == "printenv")
    {
        cout << (string)command[1] << "=";

        for(auto &it : (*cli_info)[exec_fd].env)
        {
            if(it.first == (string)command[1])
            {
                cout << it.second << "\n";
                return;
            }
        }
        cout << "\n";
        return;
    }

    //int command_num = (*cli_info)[exec_fd].command_nu;

    if(go == '|' && (*cli_info)[exec_fd].find_tmp.find(pipe_num+command_num) == (*cli_info)[exec_fd].find_tmp.end() )
    {
        //cout << command_num << " " << pipe_num << " " << pipe_num + command_num << endl;
        int fd;
        char tmpfile[] = {"/tmp/0416239XXXXXX"};
        fd = mkstemp(tmpfile);
        (*cli_info)[exec_fd].find_tmp.insert(pair<int, string >(pipe_num+command_num, (string)tmpfile));
        close(fd);
    }

    pid_t pid, wpid;
    int status;

    pid = fork();

    if( pid == 0) // child
    {

        // personal pipe
        if(go == '|')
            freopen((*cli_info)[exec_fd].find_tmp[command_num+pipe_num].c_str(), "a", stdout);
        // redirection
        else if(go == '>')
            freopen(filename.c_str(), "w", stdout);
        // pipe to other client
        else if(ssend)
        {
            stringstream tmp;
            tmp << "/tmp/0416239_" << (*cli_info)[exec_fd].ID << "_" << send_num;
            freopen(tmp.str().c_str(), "w", stdout);
        }

        // recv from other client
        if(rrecv)
        {
            stringstream tmp;
            tmp << "/tmp/0416239_" << recv_num << "_" << (*cli_info)[exec_fd].ID;
            freopen(tmp.str().c_str(), "r", stdin);
        }

        // if there's a pipe tmpfile use it as input
        else if((*cli_info)[exec_fd].find_tmp.find(command_num) != (*cli_info)[exec_fd].find_tmp.end() )
            freopen((*cli_info)[exec_fd].find_tmp[command_num].c_str(), "r", stdin);

        // change env to client env
        for(auto &it : (*cli_info)[exec_fd].env)
            setenv(it.first.c_str(), it.second.c_str(), true);

        if( execvp(command[0], command) == -1 )
        {
            dup2(exec_fd, STDOUT_FILENO);
            dup2(exec_fd, STDERR_FILENO);
            // exec failed do not creat pipe tmpfile
            if(go == '|')
            {
                struct stat statbuf;
                stat((*cli_info)[exec_fd].find_tmp[command_num+pipe_num].c_str(), &statbuf);

                // if there's a pipe tmpfile before exec failed do not delete it
                if(statbuf.st_size == 0)
                {
                    remove((*cli_info)[exec_fd].find_tmp[command_num+pipe_num].c_str());
                    (*cli_info)[exec_fd].find_tmp.erase(command_num+pipe_num);
                }
            }

            // exec failed do not creat redirection file
            else if(go == '>') remove(filename.c_str());
            cout << "Unknown command: [" << (string)command[0] << "].\n";
        }
        exit(EXIT_FAILURE);
    }
    else if ( pid < 0) perror("fork error");

    else // parent
    {
        if(rrecv)
        {
            stringstream tmp;
            tmp << "*** " << (*cli_info)[exec_fd].name << " (#" << (*cli_info)[exec_fd].ID << ") just received from " << (*cli_info)[recv_fd].name << " (#" << recv_num << ") by '" << input << "' ***\n";
            for(auto it = cli_info->begin(); it != cli_info->end(); ++it)
                send_message(tmp.str().c_str(), it->first);
        }
        if(ssend)
        {
            stringstream tmp;
            tmp << "*** " << (*cli_info)[exec_fd].name << " (#" << (*cli_info)[exec_fd].ID << ") just piped '" << input << "' to " << (*cli_info)[send_fd].name << " (#" << send_num << ") ***\n";
            for(auto it = cli_info->begin(); it != cli_info->end(); ++it)
                send_message(tmp.str().c_str(), it->first);

        }
        int receive_status;
        waitpid(pid, &status, 0);
        if(WIFEXITED(status))  receive_status = WEXITSTATUS(status);

        if(receive_status == EXIT_FAILURE)
        {
            command_not_found = true;
            if(!first_command) --command_num;
        }
        if(rrecv)
        {
            stringstream tmp;
            tmp << "/tmp/0416239_" << recv_num << "_" << (*cli_info)[exec_fd].ID;
            remove(tmp.str().c_str());
        }
        rrecv = ssend = false;

        if((*cli_info)[exec_fd].find_tmp.find(command_num) != (*cli_info)[exec_fd].find_tmp.end() )
        {
            remove((*cli_info)[exec_fd].find_tmp[command_num].c_str());
            (*cli_info)[exec_fd].find_tmp.erase(command_num);
        }
    }
}
void part(vector<char*> parse)
{
    if(ssend)
    {
        send_fd = -1;
        for(auto it = cli_info->begin(); it != cli_info->end(); ++it)
            if(it->second.ID == send_num)
            {
                send_fd = it->first;
                break;
            }
        if(send_fd == -1)
        {
            cout << "*** Error: user #" << send_num << " does not exist yet. ***\n";
            if(rrecv == false) return;
        }

        stringstream tmp;
        tmp << "/tmp/0416239_" << (*cli_info)[exec_fd].ID << "_" << send_num;
        FILE *pipe_file;

        if(pipe_file = fopen(tmp.str().c_str(), "r"))
        {
            fclose(pipe_file);
            cout << "*** Error: the pipe #" << (*cli_info)[exec_fd].ID << "->#" << send_num << " already exists. ***\n";
            if(rrecv == false) return;
        }
    }
    if(rrecv)
    {
        recv_fd = -1;
        for(auto it = cli_info->begin(); it != cli_info->end(); ++it)
            if(it->second.ID == recv_num)
            {
                recv_fd = it->first;
                break;
            }
        stringstream tmp;
        tmp << "/tmp/0416239_" << recv_num << "_" << (*cli_info)[exec_fd].ID;
        FILE *pipe_file = fopen(tmp.str().c_str(), "r");
        if(recv_fd == -1 || pipe_file == nullptr )
        {
            if(pipe_file) fclose(pipe_file);
            cout << "*** Error: the pipe #" << recv_num << "->#" << (*cli_info)[exec_fd].ID << " does not exist yet. ***\n";
            rrecv = false;
            command_not_found = true;
            return;
        }
    }
    if(parse.empty()) return;
    command_num++;
    parse.push_back(NULL);
    char** command = parse.data();
    launch(command);
    first_command = false;
}
void shell()
{
    string ini_dir = (string)getenv("HOME") + "/ras";
    chdir(ini_dir.c_str());
    setenv("PATH", "bin:." , true);
    while( (cout << "% ").flush() && getline(cin, input) )
    {
        while(*locking) sleep(0.01);
        *locking = true;

        if(input[input.size()-1] == '\n') input.pop_back();
        if(input[input.size()-1] == '\r') input.pop_back();
        if(input.empty()) continue;
        if(input == "exit")
        {
            for(auto &it : (*cli_info)[exec_fd].find_tmp) remove(it.second.c_str());
            stringstream tmp;
            tmp << "*** User '" << (*cli_info)[exec_fd].name << "' left. ***" << endl;
            for(auto it = cli_info->begin(); it != cli_info->end(); ++it)
            {
               stringstream ttmp;
               ttmp << "/tmp/0416239_" << it->second.ID << "_" << (*cli_info)[exec_fd].ID;
               remove(ttmp.str().c_str());
               send_message(tmp.str().c_str(), it->first);
            }
            used_ID[(*cli_info)[exec_fd].ID - 1] = false;
            close_child_fd(exec_fd);
            close(exec_fd);
            cli_info->erase(exec_fd);
            *locking = false;
            exit(0);
        }


        vector<char*> parse;
        stringstream ss(input);
        ssend = rrecv = command_not_found = false;
        send_num = recv_num = -1;
        first_command = true;
        while(1)
        {
            if(command_not_found) break;
            go = 0;
            string tmp;
            ss >> tmp;
            if(first_command == true)
            {
                if(tmp == "name")
                {
                    bool check = false;
                    string n;
                    ss >> n;
                    for(auto it = cli_info->begin(); it != cli_info->end(); ++it)
                    {
                        if(it->first != exec_fd && n == it->second.name)
                        {
                            cout << "*** User '" << n << "' already exists. ***\n";
                            check = true;
                            break;
                        }
                    }
                    if(check) continue;

                    stringstream tmp;
                    tmp <<"*** User from "<<(*cli_info)[exec_fd].IP<<"/" << (*cli_info)[exec_fd].port << " is named '" << n << "'. ***\n";
                    for(auto it = cli_info->begin(); it != cli_info->end(); ++it)
                        send_message(tmp.str().c_str(), it->first);
                    (*cli_info)[exec_fd].name = n;
                    continue;
                }
                if(tmp == "who")
                {
                    cout << "<ID>\t<nickname>\t<IP/port>\t<indicate me>\n";
                    for(int i = 0; i < 30; ++i)
                    {
                        if(used_ID[i] == true)
                            for(auto it = cli_info->begin(); it != cli_info->end(); ++it)
                            {
                                if(it->second.ID == i + 1)
                                {
                                    cout << it->second.ID << "\t" << it->second.name << "\t" << it->second.IP << "/" << it->second.port;
                                    if(it->first == exec_fd) cout << "\t<-me\n";
                                    else cout << "\n";
                                    break;
                                } 
                            }
                    }
                    continue;
                }
                if(tmp == "tell")
                {
                    bool check = false;
                    int id;
                    ss >> id;
                    for(auto it = cli_info->begin(); it != cli_info->end(); ++it)
                    {
                        if(it->second.ID == id)
                        {
                            char msg[MAX_MESSAGE];
                            ss.get(msg, MAX_MESSAGE);
                            stringstream tmp;
                            tmp << "*** " << (*cli_info)[exec_fd].name << " told you ***: " << msg << "\n";
                            send_message(tmp.str().c_str(), it->first);
                            check = true;
                            break;
                        }
                    }
                    if(check == false) cout << "*** Error: user #" << id << " does not exist yet. ***\n";
                    break;
                }
                if(tmp == "yell")
                {
                    char msg[MAX_MESSAGE];
                    ss.get(msg, MAX_MESSAGE);
                    stringstream tmp;
                    tmp << "*** " << (*cli_info)[exec_fd].name << " yelled ***: " << msg << "\n";
                    for(auto it = cli_info->begin(); it != cli_info->end(); ++it)
                        send_message(tmp.str().c_str(), it->first);
                    continue;
                }
            }
            if(tmp == "")
            {
                go = '\0';
                part(parse);
                break;
            }
            if(tmp[0] == '|')
            {
                go = tmp[0];
                tmp.erase(0,1);
                pipe_num = tmp.empty() ? 1 : stoi(tmp);
                part(parse);
                parse.clear();
                continue;
            }
            if(tmp == ">")
            {
                go = tmp[0];
                ss >> filename;
                part(parse);
                break;
            }
            if(tmp[0] == '>')
            {
                ssend = true;
                tmp.erase(0,1);
                send_num = stoi(tmp);
                ss >> tmp;
                if(tmp[0] == '<')
                {
                    rrecv = true;
                    tmp.erase(0,1);
                    recv_num = stoi(tmp);
                }
                part(parse);
                break;
            }
            if(tmp[0] == '<')
            {
                rrecv = true;
                tmp.erase(0,1);
                recv_num = stoi(tmp);
                continue;
            }
            char* qq = new char[tmp.length()+1];
            strcpy(qq, tmp.c_str());
            parse.push_back(qq);
        }
        *locking = false;
    }
}
void init_server(struct sockaddr_in serv_addr)
{
     sockfd = socket(AF_INET, SOCK_STREAM, 0);
     if (sockfd < 0) perror("ERROR opening socket");
     bzero((char *) &serv_addr, sizeof(serv_addr));
     serv_addr.sin_family = AF_INET;
     serv_addr.sin_addr.s_addr = INADDR_ANY;
     serv_addr.sin_port = htons(server_port);
     if (bind(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0) perror("ERROR on binding");
     listen(sockfd,30);
}
void child_exit(int)
{
    wait(nullptr);
}
void parent_exit(int)
{
    for(auto it = cli_info->begin(); it != cli_info->end(); ++it)
    {
        for(int i = 1; i < MAX_CLIENTS; ++i)
        {
            stringstream tmp;
            tmp << "/tmp/0416239_" << it->second.ID << "_" << i;
            remove(tmp.str().c_str());
        }
        for(auto &itt : it->second.find_tmp)
            remove(itt.second.c_str());
    }
    close(sockfd);
    exit(0);
}
void write_something(int)
{
    write(*write_to, write_msg, strlen(write_msg));
    *sending = false;
}
void send_message(const char *message, int fd)
{
    while(*sending) sleep(0.01);
    *sending = true;
    *write_to = fd;
    strcpy(write_msg, message);
    kill(master_pid, SIGUSR1);
    while(*sending) sleep(0.01);

}
void close_something(int)
{
    shutdown(*write_to, SHUT_RDWR);
    *sending = false;
}
void close_child_fd(int fd)
{
    while(*sending) sleep(0.01);
    *sending = true;
    *write_to = fd;
    kill(master_pid, SIGUSR2);
    while(*sending) sleep(0.01);
}
int main(int argc, char *argv[])
{
     master_pid = getpid();
     signal (SIGCHLD, child_exit);
     signal (SIGINT, parent_exit);
     signal (SIGUSR1, write_something);
     signal (SIGUSR2, close_something);

     string ini_dir = (string)getenv("HOME") + "/ras";
     chdir(ini_dir.c_str());
     setenv("PATH", "bin:." , true);

     struct sockaddr_in serv_addr;
     init_server(serv_addr);

     struct sockaddr_in cli_addr;
     socklen_t clilen;

     write_msg = (char*)mmap(NULL, MAX_MESSAGE*sizeof(char), PROT_READ | PROT_WRITE, MAP_SHARED | MAP_ANONYMOUS, -1, 0);
     memset((void*) write_msg, 0, MAX_MESSAGE*sizeof(char));

     write_to = (int*)mmap(NULL, sizeof(int), PROT_READ | PROT_WRITE, MAP_SHARED | MAP_ANONYMOUS, -1, 0);
     memset((void*) write_to, 0, sizeof(int));

     used_ID = (bool*)mmap(NULL, MAX_CLIENTS*sizeof(bool), PROT_READ | PROT_WRITE, MAP_SHARED | MAP_ANONYMOUS, -1, 0);
     memset((void*) used_ID, 0, MAX_CLIENTS*sizeof(bool));

     sending = (bool*)mmap(NULL, sizeof(bool), PROT_READ | PROT_WRITE, MAP_SHARED | MAP_ANONYMOUS, -1, 0);
     memset((void*) sending, 0, sizeof(bool));

     locking = (bool*)mmap(NULL, sizeof(bool), PROT_READ | PROT_WRITE, MAP_SHARED | MAP_ANONYMOUS, -1, 0);
     memset((void*) locking, 0, sizeof(bool));

     *sending = false;
     while(1)
     {
         clilen = sizeof(cli_addr);
         newsockfd = accept(sockfd, (struct sockaddr *) &cli_addr, &clilen);

         pid_t pid;
         pid = fork();
         if(pid == 0)
         {
            close(sockfd);
            exec_fd = newsockfd;
            dup2(newsockfd, STDOUT_FILENO);
            dup2(newsockfd, STDIN_FILENO);
            dup2(newsockfd, STDERR_FILENO);
            char hbuf[NI_MAXHOST], sbuf[NI_MAXSERV];
            getnameinfo((struct sockaddr*)&cli_addr, clilen, hbuf, sizeof(hbuf), sbuf, sizeof(sbuf), 0);
            if(demo)
            {
                bzero(hbuf, NI_MAXHOST);
                bzero(sbuf, NI_MAXSERV);
                sprintf(hbuf, "CGILAB");
                sprintf(sbuf, "511");
            }
            stringstream ss_enter;
            ss_enter << "*** User '(no name)' entered from " << hbuf << "/" << sbuf << ". ***\n";

            // add to vector
            data ini_info;
            for(int i = 0; i < 30; ++i)
            {
                if(used_ID[i] == false)
                {
                    ini_info.ID = i + 1;
                    used_ID[i] = true;
                    break;
                }
            }
            ini_info.name = "(no name)";
            ini_info.IP = (string)hbuf;
            ini_info.port = (string)sbuf;
            ini_info.env["PATH"] = "bin:.";
            cli_info->insert(pair<int, data>(newsockfd, ini_info));
            command_num = 0;


            while(*locking) sleep(0.01);
            *locking = true;

            cout << "****************************************" << endl;
            cout << "** Welcome to the information server. **" << endl;
            cout << "****************************************" << endl;

            for(auto it = cli_info->begin(); it != cli_info->end(); ++it)
                write(it->first, ss_enter.str().c_str(), ss_enter.str().size());

            *locking = false;

            shell();
            return 0;
         }
         else
         {
         }
     }
     return 0;
}
